package com.infy.social.weibodemo;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

/**
 * Created by willi on 2017/1/3.
 */

public class BaseActivity extends AppCompatActivity {

    protected Context mContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mContext = this;
    }

    public void toast(CharSequence s) {
        Toast.makeText(mContext, s, Toast.LENGTH_LONG).show();
    }
}
