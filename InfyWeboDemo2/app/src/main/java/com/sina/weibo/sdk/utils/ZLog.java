package com.sina.weibo.sdk.utils;

import android.util.Log;

/**
 * Created by Willi.Zhu on 2017/1/4.
 */
public class ZLog {

    public static String TAG = "InfySinaWeibo";

    public static void d(String content) {
        d(TAG, content);
    }

    public static void d(String TAG, String content) {
        Log.d(TAG, content);
    }
}
