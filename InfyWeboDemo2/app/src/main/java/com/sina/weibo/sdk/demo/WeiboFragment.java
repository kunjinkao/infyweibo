package com.sina.weibo.sdk.demo;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.text.Html;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.sina.weibo.sdk.auth.Oauth2AccessToken;
import com.sina.weibo.sdk.auth.WeiboAuthListener;
import com.sina.weibo.sdk.auth.sso.SsoHandler;
import com.sina.weibo.sdk.exception.WeiboException;
import com.sina.weibo.sdk.net.RequestListener;
import com.sina.weibo.sdk.openapi.StatusesAPI;
import com.sina.weibo.sdk.openapi.UsersAPI;
import com.sina.weibo.sdk.openapi.models.ErrorInfo;
import com.sina.weibo.sdk.openapi.models.Status;
import com.sina.weibo.sdk.openapi.models.StatusList;
import com.sina.weibo.sdk.openapi.models.User;
import com.sina.weibo.sdk.utils.AccessTokenKeeper;
import com.sina.weibo.sdk.utils.Constants;
import com.sina.weibo.sdk.utils.ZImageLoader;
import com.sina.weibo.sdk.utils.ZLog;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


public class WeiboFragment extends Fragment {

    private ContainerActivity mActivity;
    private Context mContext;

    /**
     * 封装了 "access_token"，"expires_in"，"refresh_token"，并提供了他们的管理功能
     */
    private Oauth2AccessToken mAccessToken;
    /**
     * 用户信息接口
     */
    private UsersAPI mUsersAPI;
    /**
     * 用于获取微博信息流等操作的API
     */
    private StatusesAPI mStatusesAPI;

    private void toast(String str) {
        Toast.makeText(mContext, str, Toast.LENGTH_LONG).show();
    }

    private void log(String str) {
        ZLog.d(str);
    }

    private View mRootView;
    private WeiboListAdapter mAdapter;
    private List<Status> mStatusList;

    private ProgressDialog mLoadingDialog;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = getActivity();
        mActivity = (ContainerActivity) getActivity();

        setHasOptionsMenu(true);

        ZImageLoader.init(mContext);
        mStatusList = new ArrayList<>();
        mAdapter = new WeiboListAdapter();

        mLoadingDialog = new ProgressDialog(mActivity);
        mLoadingDialog.setMessage("Loading data...");
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.add("logout")
                .setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        confirmLogout();
                        return true;
                    }
                }).setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
    }

    private void confirmLogout() {
        new AlertDialog.Builder(mContext)
                .setTitle("Logout")
                .setMessage("Once logout, you will never see your timeline again.")
                .setPositiveButton("YES", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        logout();
                    }
                }).setNegativeButton("NO", null).show();
    }

    private void logout() {
        if (mAccessToken == null) {
            toast("You should login first.");
            return;
        }

        AccessTokenKeeper.clear(mContext);
        mAccessToken = null;
        showLogin();

        try {
            mActivity.getSupportActionBar().setTitle(R.string.app_name);
        } catch (Exception e) {
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.fragment_weibo, container, false);
        ListView lv = (ListView) findViewById(R.id.lv_weibo_timeline);
        lv.setDivider(new ColorDrawable(Color.argb(255, 245, 245, 245)));
        final float scale = getResources().getDisplayMetrics().density;
        int _12dp = (int) (12 * scale + 0.5f);
        lv.setDividerHeight(_12dp);
        lv.setAdapter(mAdapter);
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Status status = mStatusList.get(position);
                StringBuilder sb = new StringBuilder();
                sb.append("http://api.weibo.com/2/statuses/go");
                sb.append("?");
                sb.append("uid=" + status.user.id);
                sb.append("&");
                sb.append("id=" + status.id);
                sb.append("/");
                String url = sb.toString();
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                startActivity(intent);
            }
        });

        return mRootView;
    }

    private View findViewById(int id) {
        return mRootView.findViewById(id);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        showLogin();

        initSinaWeibo();
    }

    private void initSinaWeibo() {
        // 获取当前已保存过的 Token
        mAccessToken = AccessTokenKeeper.readAccessToken(mContext);

        if (mAccessToken != null && mAccessToken.isSessionValid()) {
            invisibleLogin();
            fetchUserData();
        }
    }

    private void invisibleLogin() {
        findViewById(R.id.layout_weibo_login).setVisibility(View.INVISIBLE);
    }

    private void showLogin() {
        findViewById(R.id.layout_weibo_login).setVisibility(View.VISIBLE);
        findViewById(R.id.btn_weibo_login).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mActivity.weiboAuth(new AuthListener());
            }
        });
    }

    /**
     * 微博认证授权回调类。
     * 1. SSO 授权时，需要在 {@link #onActivityResult} 中调用 {@link SsoHandler#authorizeCallBack} 后，
     * 该回调才会被执行。
     * 2. 非 SSO 授权时，当授权结束后，该回调就会被执行。
     * 当授权成功后，请保存该 access_token、expires_in、uid 等信息到 SharedPreferences 中。
     */
    private class AuthListener implements WeiboAuthListener {

        @Override
        public void onComplete(Bundle values) {
            // 从 Bundle 中解析 Token
            mAccessToken = Oauth2AccessToken.parseAccessToken(values);
            //从这里获取用户输入的 电话号码信息
            String phoneNum = mAccessToken.getPhoneNum();
            if (mAccessToken.isSessionValid()) {
                // 显示 Token
//                updateTokenView(false);

                // 保存 Token 到 SharedPreferences
                AccessTokenKeeper.writeAccessToken(mContext, mAccessToken);
                onLoginSucceed();
            } else {
                // 以下几种情况，您会收到 Code：
                // 1. 当您未在平台上注册的应用程序的包名与签名时；
                // 2. 当您注册的应用程序包名与签名不正确时；
                // 3. 当您在平台上注册的包名和签名与您当前测试的应用的包名和签名不匹配时。
                String code = values.getString("code");
                String message = "auth failed...";
                if (!TextUtils.isEmpty(code)) {
                    message = message + "\nObtained the code: " + code;
                }
                toast(message);
            }
        }

        @Override
        public void onCancel() {
            toast("User canceled...");
        }

        @Override
        public void onWeiboException(WeiboException e) {
            toast("Auth exception : " + e.getMessage());
        }
    }

    private void onLoginSucceed() {
        invisibleLogin();
        // 获取当前已保存过的 Token
        mAccessToken = AccessTokenKeeper.readAccessToken(mContext);

        fetchUserData();
    }

    private void fetchUserData() {
        mLoadingDialog.show();

        mUsersAPI = new UsersAPI(mContext, Constants.APP_KEY, mAccessToken);
        mStatusesAPI = new StatusesAPI(mContext, Constants.APP_KEY, mAccessToken);

        getUser();
        getWeibo();
    }

    /**
     * Simple implemention of err
     */
    private abstract class AbsResponse implements RequestListener {

        @Override
        public void onWeiboException(WeiboException e) {
            mLoadingDialog.dismiss();

            log(e.getMessage());
            ErrorInfo info = ErrorInfo.parse(e.getMessage());
            toast(info.toString());
        }
    }

    public void getUser() {
        long uid = Long.parseLong(mAccessToken.getUid());
        mUsersAPI.show(uid, new AbsResponse() {
            @Override
            public void onComplete(String response) {
                if (!TextUtils.isEmpty(response)) {
                    log(response);
                    // 调用 User#parse 将JSON串解析成User对象
                    User user = User.parse(response);
                    if (user != null) {
                        setUser(user);
                    } else {
                        toast(response);
                    }
                }
            }
        });
    }

    private void setUser(User user) {
        try {
            mActivity.getSupportActionBar().setTitle(user.screen_name);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void getWeibo() {
        mStatusesAPI.friendsTimeline(0L, 0L, 10, 1, false, 0, false, new AbsResponse() {
            @Override
            public void onComplete(String response) {
                mLoadingDialog.dismiss();

                if (!TextUtils.isEmpty(response)) {
                    log(response);
                    if (response.startsWith("{\"statuses\"")) {
                        // 调用 StatusList#parse 解析字符串成微博列表对象
                        StatusList statuses = StatusList.parse(response);
                        if (statuses != null && statuses.total_number > 0) {
                            makeTimelineDatas(statuses.statusList);
                        }
                    } else if (response.startsWith("{\"created_at\"")) {
                        // 调用 Status#parse 解析字符串成微博对象
                        Status status = Status.parse(response);
                        toast("发送一送微博成功, id = " + status.id);
                    } else {
                        toast(response);
                    }
                }
            }
        });
    }

    private void makeTimelineDatas(ArrayList<Status> statusList) {
        mStatusList.clear();
        mStatusList.addAll(statusList);

        mAdapter.notifyDataSetChanged();
    }

    private class WeiboListAdapter extends BaseAdapter {
        private LayoutInflater mInflater;

        public WeiboListAdapter() {
            mInflater = LayoutInflater.from(mContext);
        }

        @Override
        public int getCount() {
            return mStatusList.size();
        }

        @Override
        public Status getItem(int position) {
            return mStatusList.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            Holder h;
            if (convertView == null) {
                h = new Holder();
                convertView = mInflater.inflate(R.layout.item_list_weibo, null);
                h.retweetText = (TextView) convertView.findViewById(R.id.tv_item_list_weibo_retweet_text);
                h.retweetPic = (ImageView) convertView.findViewById(R.id.iv_item_list_weibo_retweet_photo);
                h.retweetLayout = convertView.findViewById(R.id.layout_item_list_weibo_retweet);
                h.author = (TextView) convertView.findViewById(R.id.tv_item_list_weibo_author);
                h.at = (TextView) convertView.findViewById(R.id.tv_item_list_weibo_at);
                h.time = (TextView) convertView.findViewById(R.id.tv_item_list_weibo_time);
                h.avatar = (ImageView) convertView.findViewById(R.id.iv_item_list_weibo_avatar);
                h.pic = (ImageView) convertView.findViewById(R.id.iv_item_list_weibo_photo);
                h.text = (TextView) convertView.findViewById(R.id.tv_item_list_weibo_text);
                h.count_retweet = (TextView) convertView.findViewById(R.id.tv_item_list_weibo_count_retweet);
                h.count_comment = (TextView) convertView.findViewById(R.id.tv_item_list_weibo_count_comment);
                h.count_like = (TextView) convertView.findViewById(R.id.tv_item_list_weibo_count_like);

                convertView.setTag(h);
            } else {
                h = (Holder) convertView.getTag();
            }

            Status status = getItem(position);
            if (status.retweeted_status != null) {
                h.retweetLayout.setVisibility(View.VISIBLE);

                Status rStatus = status.retweeted_status;
                h.retweetText.setText("来自 " + rStatus.user.screen_name + ": " + Html.fromHtml(rStatus.text));

                if (!TextUtils.isEmpty(rStatus.bmiddle_pic)) {
                    ZImageLoader.displayImage(rStatus.bmiddle_pic, h.retweetPic);
                    h.pic.setVisibility(View.VISIBLE);
                } else {
                    h.pic.setVisibility(View.GONE);
                }
            } else {
                h.retweetLayout.setVisibility(View.GONE);
            }

            User author = status.user;
            ZImageLoader.displayImageDefault(author.profile_image_url, h.avatar);
            h.author.setText(author.screen_name);
            h.at.setText("@" + author.location);
            h.time.setText(timeConvert(status.created_at));

            if (!TextUtils.isEmpty(status.bmiddle_pic)) {
                ZImageLoader.displayImage(status.bmiddle_pic, h.pic);
                h.pic.setVisibility(View.VISIBLE);
            } else {
                h.pic.setVisibility(View.GONE);
            }

            h.text.setText(Html.fromHtml(status.text));

            h.count_retweet.setText(status.reposts_count + "");
            h.count_comment.setText(status.comments_count + "");
            h.count_like.setText(status.attitudes_count + "");

            return convertView;
        }

        private String timeConvert(String orgStr) {
            try {
                SimpleDateFormat orgSdf = new SimpleDateFormat("EEE MMM dd HH:mm:ss Z yyyy");
                Date date = orgSdf.parse(orgStr);

                SimpleDateFormat sdf = new SimpleDateFormat("MMM dd");
                return sdf.format(date);
            } catch (ParseException e) {
                return "UNKNOWN";
            }
        }

        private class Holder {
            TextView author;
            TextView at;
            TextView time;
            ImageView avatar;
            ImageView pic;
            TextView text;

            View retweetLayout;
            TextView retweetText;
            ImageView retweetPic;

            TextView count_retweet;
            TextView count_comment;
            TextView count_like;
        }
    }
}