package com.sina.weibo.sdk.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Handler;
import android.widget.ImageView;

import com.nostra13.universalimageloader.cache.disc.impl.UnlimitedDiskCache;
import com.nostra13.universalimageloader.cache.disc.naming.HashCodeFileNameGenerator;
import com.nostra13.universalimageloader.cache.memory.impl.LruMemoryCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;
import com.nostra13.universalimageloader.core.decode.BaseImageDecoder;
import com.nostra13.universalimageloader.core.display.SimpleBitmapDisplayer;
import com.nostra13.universalimageloader.core.download.BaseImageDownloader;
import com.nostra13.universalimageloader.core.imageaware.ImageViewAware;
import com.nostra13.universalimageloader.utils.StorageUtils;
import com.sina.weibo.sdk.demo.R;

import java.io.File;

/**
 * Created by Willi.Zhu on 2017/1/6.
 * <p>
 * see https://github.com/nostra13/Android-Universal-Image-Loader
 * <p>
 * "http://site.com/image.png" // from Web
 * "file:///mnt/sdcard/image.png" // from SD card
 * "file:///mnt/sdcard/video.mp4" // from SD card (video thumbnail)
 * "content://media/external/images/media/13" // from content provider
 * "content://media/external/video/media/13" // from content provider (video thumbnail)
 * "assets://image.png" // from assets
 * "drawable://" + R.drawable.img // from drawables (non-9patch images)
 */

public class ZImageLoader {
    private ZImageLoader() {
    }

    private static boolean isInited = false;

    public static void init(Context context) {
        if (isInited) return;

//        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(this)
//        ...
//        .build();
//        ImageLoader.getInstance().init(config);
        File cacheDir = StorageUtils.getCacheDirectory(context);
        ImageLoaderConfiguration.Builder builder = new ImageLoaderConfiguration.Builder(context);
        builder.memoryCacheExtraOptions(480, 800);
        builder.diskCacheExtraOptions(480, 800, null);
        builder.threadPoolSize(3);
        builder.threadPriority(Thread.NORM_PRIORITY - 2);
        builder.tasksProcessingOrder(QueueProcessingType.FIFO);
        builder.denyCacheImageMultipleSizesInMemory();
        builder.memoryCache(new LruMemoryCache(2 * 1024 * 1024));
        builder.memoryCacheSize(2 * 1024 * 1024);
        builder.memoryCacheSizePercentage(13);
        builder.diskCache(new UnlimitedDiskCache(cacheDir));
        builder.diskCacheSize(50 * 1024 * 1024);
        builder.diskCacheFileCount(100);
        builder.diskCacheFileNameGenerator(new HashCodeFileNameGenerator());
        builder.imageDownloader(new BaseImageDownloader(context));
        builder.imageDecoder(new BaseImageDecoder(false));
        builder.defaultDisplayImageOptions(DisplayImageOptions.createSimple());
//        builder.writeDebugLogs();
        ImageLoaderConfiguration config = builder.build();
        ImageLoader.getInstance().init(config);
        isInited = true;
    }

    public static final DisplayImageOptions SIMPLE_DISPLAY_OPTIONS = new DisplayImageOptions.Builder()
            //  .showImageOnLoading(R.drawable.ic_stub) // resource or drawable
            // .showImageForEmptyUri(R.drawable.ic_empty) // resource or drawable
            // .showImageOnFail(R.drawable.ic_error) // resource or drawable
            .resetViewBeforeLoading(false)  // default
            .delayBeforeLoading(1000)
            .cacheInMemory(false) // default
            .cacheOnDisk(false) // default
            .considerExifParams(false) // default
            .imageScaleType(ImageScaleType.IN_SAMPLE_POWER_OF_2) // default
            .bitmapConfig(Bitmap.Config.ARGB_8888) // default
            .displayer(new SimpleBitmapDisplayer()) // default
            .handler(new Handler()) // default
            .build();

    public static final DisplayImageOptions DEFAULT_DISPLAY_OPTIONS = new DisplayImageOptions.Builder()
            .showImageOnLoading(R.mipmap.img_src_default) // resource or drawable
            .showImageForEmptyUri(R.mipmap.img_src_default) // resource or drawable
            .showImageOnFail(R.mipmap.img_src_default) // resource or drawable
            .resetViewBeforeLoading(false)  // default
            .delayBeforeLoading(1000)
            .cacheInMemory(false) // default
            .cacheOnDisk(false) // default
            .considerExifParams(false) // default
            .imageScaleType(ImageScaleType.IN_SAMPLE_POWER_OF_2) // default
            .bitmapConfig(Bitmap.Config.ARGB_8888) // default
            .displayer(new SimpleBitmapDisplayer()) // default
            .handler(new Handler()) // default
            .build();

    public static ImageLoader imageLoader = ImageLoader.getInstance(); // Get singleton instance


    public static void displayImage(String url, ImageView iv) {
        imageLoader.displayImage(url, iv);
    }

    public static void displayImageDefault(String url, ImageView iv) {
        imageLoader.displayImage(url, iv, DEFAULT_DISPLAY_OPTIONS);
    }
}
