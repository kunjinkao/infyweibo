package com.sina.weibo.sdk.demo;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import com.sina.weibo.sdk.utils.ZLog;

/**
 * Created by willi on 2017/1/3.
 */

@Deprecated
public class BaseActivity extends AppCompatActivity {

    protected Context mContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mContext = this;
    }

    public void log(String content) {
        ZLog.d(content);
    }

    public void toast(CharSequence s) {
        Toast.makeText(mContext, s, Toast.LENGTH_LONG).show();
    }
}
