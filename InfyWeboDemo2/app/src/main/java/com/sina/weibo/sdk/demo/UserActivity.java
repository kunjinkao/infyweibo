package com.sina.weibo.sdk.demo;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.sina.weibo.sdk.auth.Oauth2AccessToken;
import com.sina.weibo.sdk.exception.WeiboException;
import com.sina.weibo.sdk.net.RequestListener;
import com.sina.weibo.sdk.openapi.StatusesAPI;
import com.sina.weibo.sdk.openapi.models.ErrorInfo;
import com.sina.weibo.sdk.openapi.models.Status;
import com.sina.weibo.sdk.openapi.models.StatusList;
import com.sina.weibo.sdk.openapi.models.User;
import com.sina.weibo.sdk.utils.AccessTokenKeeper;
import com.sina.weibo.sdk.utils.Constants;
import com.sina.weibo.sdk.openapi.UsersAPI;

import java.util.ArrayList;
import java.util.List;

@Deprecated
public class UserActivity extends BaseActivity {

    /**
     * 当前 Token 信息
     */
    private Oauth2AccessToken mAccessToken;
    /**
     * 用户信息接口
     */
    private UsersAPI mUsersAPI;
    /**
     * 用于获取微博信息流等操作的API
     */
    private StatusesAPI mStatusesAPI;

    private ArrayAdapter<String> mListAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user);

        mListAdapter = new ArrayAdapter<>(mContext, android.R.layout.simple_list_item_1, new ArrayList<String>());

        ListView lv = (ListView) findViewById(R.id.list);
        lv.setAdapter(mListAdapter);

        // 获取当前已保存过的 Token
        mAccessToken = AccessTokenKeeper.readAccessToken(this);

        mUsersAPI = new UsersAPI(mContext, Constants.APP_KEY, mAccessToken);
        mStatusesAPI = new StatusesAPI(mContext, Constants.APP_KEY, mAccessToken);

        getUser();
        getWeibo();
    }

    public void getUser() {
        long uid = Long.parseLong(mAccessToken.getUid());
        mUsersAPI.show(uid, new AbsResponse() {
            @Override
            public void onComplete(String response) {
                if (!TextUtils.isEmpty(response)) {
                    log(response);
                    // 调用 User#parse 将JSON串解析成User对象
                    User user = User.parse(response);
                    if (user != null) {
                        toast("用户昵称：" + user.screen_name);

                        setUser(user);
                    } else {
                        toast(response);
                    }
                }
            }
        });
    }

    private void setUser(User user) {
        TextView tv = (TextView) findViewById(R.id.user);
        StringBuffer sb = new StringBuffer();
        sb.append("--- owner ---");
        sb.append("\n");
        sb.append("id: " + user.idstr);
        sb.append("\n");
        sb.append("nick: " + user.screen_name);
        sb.append("\n");
        sb.append("avatar: " + user.avatar_large);
        sb.append("\n");
        sb.append("--- ----- ---");
        tv.setText(sb.toString());
    }

    public void getWeibo() {
        mStatusesAPI.friendsTimeline(0L, 0L, 10, 1, false, 0, false, new AbsResponse() {
            @Override
            public void onComplete(String response) {
                if (!TextUtils.isEmpty(response)) {
                    log(response);
                    if (response.startsWith("{\"statuses\"")) {
                        // 调用 StatusList#parse 解析字符串成微博列表对象
                        StatusList statuses = StatusList.parse(response);
                        if (statuses != null && statuses.total_number > 0) {
                            toast("获取微博信息流成功, 条数: " + statuses.statusList.size());

                            setDatas(statuses.statusList);
                        }
                    } else if (response.startsWith("{\"created_at\"")) {
                        // 调用 Status#parse 解析字符串成微博对象
                        Status status = Status.parse(response);
                        toast("发送一送微博成功, id = " + status.id);
                    } else {
                        toast(response);
                    }
                }
            }
        });
    }

    private void setDatas(ArrayList<Status> statusList) {
        for (Status status : statusList) {
            StringBuffer sb = new StringBuffer();
            sb.append("author:" + status.user.screen_name);
            sb.append("\n");
            sb.append("create:" + status.created_at);
            sb.append("\n");
            sb.append("comment:" + status.comments_count);
            sb.append("\n");
            sb.append("forward:" + status.reposts_count);
            sb.append("\n");
            sb.append("like:" + status.attitudes_count);
            sb.append("\n");
            sb.append("text:" + status.text);
            if (status.pic_urls != null && status.pic_urls.size() > 0) {
                sb.append("\n");
                sb.append("pics:" + status.pic_urls);
            }

            Status retweed = status.retweeted_status;
            if (retweed != null) {
                sb.append("\n");
                sb.append(">>> >>> retweet >>> >>>");
                sb.append("\n");
                sb.append(retweed.user.screen_name + " : " + retweed.text);
                sb.append("\n");
                sb.append(">>> >>> >>> >>> >>> >>>");
            }
            log(sb.toString());
            mListAdapter.add(sb.toString());
        }
    }

    private abstract class AbsResponse implements RequestListener {

        @Override
        public void onWeiboException(WeiboException e) {
            log(e.getMessage());
            ErrorInfo info = ErrorInfo.parse(e.getMessage());
            toast(info.toString());
        }
    }
}